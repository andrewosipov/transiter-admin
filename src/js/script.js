﻿$(document).ready(function () {

    $('.form__point-a .form__order-input, .form__point-b .form__order-input').on('click', function () {
        $(this).parent().parent().find('.form__field_focused').removeClass('form__field_focused');
        $(this).parent().addClass('form__field_focused');
    });

    // переключатель контента
    $('.toggle__target').on('click', function () {
        $('.toggle__content.' + $(this).data('toggle-group') ).removeClass('toggle__content_active');
        $('.toggle__content.' + $(this).data('toggle-group') + '.' + $(this).data('toggle-target') ).addClass('toggle__content_active');
        //return false;
    });

    $('.extra-navbar__mobile-menu-toogler').on('click', function () {
        $(this).toggleClass('open');
        $('.extra-navbar__menu').toggleClass('mobile-open');
        return false;
    });

    $('.header-nav__mobile-menu-toogler').on('click', function () {
        $(this).toggleClass('open');
        $('.header-nav').toggleClass('mobile-open');
        return false;
    });

    $('.form__search_icon').on('click', function () {
        $(this).toggleClass('open');
        $('.form__search').toggleClass('open');
        $('.form__search .form__order-input').focus();
        return false;
    });

    if( $('.map-container__map').length>0 ){

    }

    $('.form__order-city').select2();
    $('.form__order-select:not(.form__order-select_search)').select2({
        minimumResultsForSearch: Infinity
    });
    $('.form__order-select_search').select2();

    $('.form__order-time').inputmask("h:s",{ "placeholder": "чч/мм" });//.mask("99:99");
    $('.form__order-date').ionDatePicker({
        lang: "ru",
        sundayFirst: false,
        years: "80",
        format: "DD.MM.YYYY",
        onClick: function(date){
            //$("#result-1").html("onClick: " + date);
        }
    });

    if( typeof chartOptions !== 'undefined' ){
        $(".statistic__graph").CanvasJSChart(chartOptions);
    }

    $('.modal.open').modal('show');

});

$(window).on('scroll', function () {

});

$(window).on('load', function(){
	
	 check_footer();
		
});

$(window).on('resize', function(){


	 check_footer();
		
});

function check_footer(){

    if( $('.map-form-container').height() <= $('.map-container').height() ){
        $('.map-container').css({ height: $(window).height()-80 });
	}else{
        $('.map-container').css({ height: $('.map-form-container').height()+50 });
	}

	//$('.container-scroll').css({ height: $(window).height()-150 });
	
	//$('.footer').show();
}